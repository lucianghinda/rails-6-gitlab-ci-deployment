FROM ruby:2.7-alpine
MAINTAINER Lucian Ghinda

RUN apk add --no-cache --update build-base
RUN apk add --no-cache \
      curl \
      yarn \
      nodejs \
      postgresql \
      postgresql-contrib \
      libpq \
      cmake \
      build-base \
      unzip \
      postgresql-client \
      bash \
      postgresql-dev \
      git

# Install Chrome
RUN apk add --no-cache chromium

# Install chromedriver
RUN apk add --no-cache chromium-chromedriver

# Install heroku-cli
RUn curl https://cli-assets.heroku.com/install.sh | sh

# Install dpl and heroku-cli
RUN gem install dpl

RUN gem install bundler:2.1.4
