# frozen_string_literal: true

require 'test_helper'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase

  options = {
    desired_capabilities: {
      chromeOptions: {
        args: %w[headless disable-gpu disable-dev-shm-usage]
      }
    }
  }

  if ENV.key?("CI_EXECUTION") && ENV["CI_EXECUTION"] == true
    options[:url] = ENV['SELENIUM_REMOTE_URL'] if ENV['SELENIUM_REMOTE_URL']
    Capybara.server_port = 8200
  end

  driven_by(
    :selenium,
    using: :chrome,
    screen_size: [1400, 1400],
    options: options
  )

  def remove_uploaded_files
    FileUtils.rm_rf(Rails.root.join("tmp/storage"))
  end

  def after_teardown
    super
    remove_uploaded_files
  end
end

Capybara.server = :puma, { Silent: true }
